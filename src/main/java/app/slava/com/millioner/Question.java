package app.slava.com.millioner;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

/**
 * Created by slava on 11.10.15.
 */
public class Question {
    private String question;
    private String[] answer;

   private int trueAnswer;


    public Question(Cursor c, int nmbQuestion) throws IOException {

        c.moveToPosition(nmbQuestion);

//        берем вопрос из 1ой колонки
        question = c.getString(1);

//        массив ответов
        answer = new String[4];

        for(int i = 0; i<answer.length; i++){

//            2+i так как ответы хранятся во 2ой колонке в базе
            answer[i] = c.getString(2+i);
        }
        trueAnswer = c.getInt(c.getColumnCount() - 1);


    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswer() { return answer; }

    public String getTrueAnswerText(){
        return answer[getTrueAnswer()-1];
    }

    public int getTrueAnswer(){ return trueAnswer; }
}
