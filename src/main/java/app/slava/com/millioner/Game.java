package app.slava.com.millioner;

import android.database.Cursor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by slava on 16.10.15.
 */
public class Game {
    private Question question;
    private int nmbQuest = 0;
    private ArrayList<Integer> nmbQuestions;
    private DataBaseHelper mDbHelper;
    private Cursor mCursor;
    static int counter = 0;


    public Game( DataBaseHelper mDbHelper, Cursor mCursor) {

        this.mDbHelper = mDbHelper;
        this.mCursor = mCursor;

        nmbQuestions = new ArrayList<Integer>();
    }

    public Question getQuestion() {
        return question;
    }


    public void createContent() {
        try {
            checkContain();
            question = new Question(mCursor, nmbQuest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkContain(){
        Random random;
        random = new Random();

//       вопрос достается из БД рандомно. Рандом из количество строк в таблице
        nmbQuest = random.nextInt(mCursor.getCount());

//        делаем проверку, чтоб вопросы не повторялись
        if(nmbQuestions.contains(nmbQuest)) {
            nmbQuest = random.nextInt(mCursor.getCount());
            checkContain();
        }
        else
            nmbQuestions.add(nmbQuest);
    }

    public ArrayList<Integer> getNmbQuestions() {
        return nmbQuestions;
    }

    public void setmCursor(Cursor mCursor) {
        this.mCursor = mCursor;
    }


}
