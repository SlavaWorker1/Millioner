package app.slava.com.millioner;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends Activity implements View.OnClickListener {

    TextView tvQuest, tvPrize;
    Button[] button;

    ImageButton imbCall, imbPeople, imbFifty;

    int[] id;
    Cursor mCursor;
    DataBaseHelper mDbHelper;
    Game game;
    Helpers helpers;
    ShowInfo showInfo;


    String[] lvlQuestion;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lvlQuestion = new String[3];

//        Массив строк, название таблиц в БД
        lvlQuestion[0] = "First";
        lvlQuestion[1] = "Second";
        lvlQuestion[2] = "Third";

        imbCall = (ImageButton)findViewById(R.id.imbCall);
        imbFifty = (ImageButton)findViewById(R.id.imbFifty);
        imbPeople = (ImageButton)findViewById(R.id.imbPeople);


        tvQuest = (TextView) findViewById(R.id.tvQuestion);
        tvPrize = (TextView) findViewById(R.id.tvPrize);



        button = new Button[4];

        id = new int[]{
                R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4
        };

        for(int i = 0; i < button.length; i++){
            button[i]=(Button)findViewById(id[i]);
            button[i].setOnClickListener(this);
            button[i].setTag(i+1);
        }

        try {
            mCursor = createDatabase(lvlQuestion[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        game = new Game(mDbHelper, mCursor);
        game.createContent();


//        класс для работы с подсказками
        helpers = new Helpers(game,button,imbCall, imbFifty, imbPeople, this);

//        класс для отображения информации, показа диаологов
        showInfo = new ShowInfo(button, tvQuest, tvPrize, mCursor, game, this);
        showInfo.setText();
    }


    @Override
    public void onClick(View view) {

        showInfo.showUserAnswer(view);
                showInfo.showTrueAnswer(showInfo.getTrueAnswer());
                showInfo.getSelected().setTag(view.getTag());

//        счетчик вопросов, если достигает определенного значения, переходим на след.уровень сложности
                Game.counter++;

                if (Game.counter == 3)
                    try {

                        mCursor = createDatabase(lvlQuestion[1]);

                        game.setmCursor(createDatabase(lvlQuestion[1]));

//                       при переходи на новый уровень, очищаем список использовавшихся вопросов
                        game.getNmbQuestions().clear();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                if (Game.counter == 5) {
                    try {
                        mCursor = createDatabase(lvlQuestion[2]);
                        game.setmCursor(createDatabase(lvlQuestion[2]));
                        game.getNmbQuestions().clear();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                if (Game.counter == 7) {
                    showInfo.showDialog("Поздравляем вы стали миллионером", true);
                    showInfo.tvPrize.clearComposingText();
                    game.getNmbQuestions().clear();

                }
                    for (int i = 0; i < button.length; i++)
                        button[i].setClickable(false);

        }


    @Override
    protected void onDestroy() {
        Game.counter = 0;
        super.onDestroy();
    }

    public Cursor createDatabase(String lvlQuestion) throws IOException {
            mDbHelper = new DataBaseHelper(this);
            mDbHelper.createDataBase();
            mDbHelper.openDataBase();
            db = mDbHelper.getWritableDatabase();
            Cursor cursor;
            cursor = db.rawQuery("SELECT * FROM " + lvlQuestion , null);
            return cursor;
    }



}
