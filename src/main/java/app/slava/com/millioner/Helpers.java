package app.slava.com.millioner;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Random;

/**
 * Created by slava on 26.10.15.
 */
public class Helpers implements View.OnClickListener {

    ImageButton imbCall, imbFifty, imbPeople;
    Button button[];
    Context context;
    Game game;

    public Helpers(final Game game, final Button[] button, ImageButton imbCall,
                   ImageButton imbFifty, ImageButton imbPeople, final Context context) {
        this.game = game;
        this.button = button;
        this.imbCall = imbCall;
        this.imbFifty = imbFifty;
        this.imbPeople = imbPeople;
        this.context = context;

        imbFifty.setOnClickListener(this);
        imbCall.setOnClickListener(this);
        imbPeople.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imbFifty:


                Random random = new Random();
                int fake = random.nextInt(4);
//                проверяем, чтоб неправильный ответ не был равен 0 и правильному ответу.
                while (fake==game.getQuestion().getTrueAnswer()||fake==0){
                    fake = random.nextInt(4);
                }

//                отключаем кнопки
                for(int j = 0; j < button.length;j++){
                    if(button[j].getTag()!=game.getQuestion().getTrueAnswer()&&button[j].getTag()!=fake) {
                        button[j].setClickable(false);
                        button[j].setText(" ");
                    }
                }

                break;
            case R.id.imbCall:
                context.startActivity(new Intent(Intent.ACTION_DIAL));

                break;

            case R.id.imbPeople:
                    int peopleAnswers[];
                    peopleAnswers = new int[4];
                    peopleAnswers[0] = randomBetween(65, 51);
                    peopleAnswers[1] = randomBetween(25, 15);
                    peopleAnswers[2] = randomBetween(7, 4);
                    peopleAnswers[3] = 100 - peopleAnswers[0] - peopleAnswers[1] - peopleAnswers[2];
                    Log.d("CheckCheck", peopleAnswers[0] + " " + peopleAnswers[1] + " " + peopleAnswers[2] + " " + peopleAnswers[3]);

                    ShowInfo showInfo = new ShowInfo(peopleAnswers,context,game);
                    showInfo.showDialog(peopleAnswers);

                break;
        }

        view.setClickable(false);
    }

    public int randomBetween(int high, int low){
        Random r = new Random();
        return r.nextInt(high-low) + low;
    }

}



