package app.slava.com.millioner;

/**
 * Created by slava on 30.10.15.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ShowInfo {
    Button button[];
    TextView tvQuestion, tvPrize;
    View trueAnswer, selected;
    Game game;
    Handler handler;
    Boolean bHandl = true;
    Thread thread;
    Context context;
    Cursor cursor;
    String[] prize = {"0", "1000", "10 000", "50 000", "100 000", "250 000", "500 000", "1 000 000"};
    int peopleAnswers[];


    public ShowInfo(Button[] button, TextView tvQuestion, TextView tvPrize,
                    Cursor cursor, final Game game, Context baseContext) {
        this.button = button;
        this.tvQuestion = tvQuestion;
        this.tvPrize = tvPrize;
        this.cursor = cursor;
        this.game = game;

        this.context = baseContext;
        this.handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

//                подсвечиваем правильный ответ
//                bHandle - переменная флаг, для мигания
                if (bHandl) {

                    if(msg.what!=game.getQuestion().getTrueAnswer()) {
                        getTrueAnswer().setBackgroundResource(R.drawable.button);
                    }
                    else {
                        getTrueAnswer().setBackgroundResource(R.drawable.button_yellow);
                    }

                    bHandl = false;

                } else {
                    getTrueAnswer().setBackgroundResource(R.drawable.button_green);
                    bHandl = true;
                }


//                если поток умер переходим к след. вопросу или показaваем диалог
                if(!thread.isAlive()){
                    if (msg.what == game.getQuestion().getTrueAnswer()) {

                        Log.d("counter", String.valueOf(Game.counter));

                        game.createContent();

                        setText();

                    } else {

                        showDialog("Вы проиграли", false);
                        Log.d("CheckThread","Погиб поток, невольник чести");
                    }
                }
            }
        };
    }

    public ShowInfo(int peopleAnswers[], Context context, Game game ){
        this.peopleAnswers = peopleAnswers;
        this.context = context;
        this.game = game;

    }

    public void setText() {
        for(int i = 0; i < button.length; i++)
        {
            button[i].setClickable(true);
            button[i].setBackgroundResource(R.drawable.button);
            tvPrize.setText("Ваш выигрышь: "+ prize[Game.counter]);
        }

        tvQuestion.setText(game.getQuestion().getQuestion());
        for (int i = 0; i < button.length; i++) {
            button[i].setText(game.getQuestion().getAnswer()[i]);

            if (game.getQuestion().getTrueAnswer() == i + 1) {
                trueAnswer = button[i];
                trueAnswer.setTag(i+1);
            }
        }
    }


    public void showUserAnswer(View view) {
        view.setBackgroundResource(R.drawable.button_yellow);
    }



    public void showTrueAnswer(final View view) {
        selected = view;
        thread  = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(200);
                        handler.sendEmptyMessage((int) view.getTag());
                        if(i==10)
                            thread.join();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }


    public void showDialog(String result, boolean right){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);


        builder.setTitle(checkNumber(result, right))
                .setMessage("Хотите начать новую игру?")
                .setCancelable(false)
                .setPositiveButton("Да, хочу", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        game.getNmbQuestions().clear();
                        game.setmCursor(cursor);
                        game.createContent();
                        Game.counter=0;
                        tvPrize.clearComposingText();

                        setText();
                    }
                })
                .setNegativeButton("Нет, спасибо",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((Activity) context).finish();
                            }
                        });


        AlertDialog alert = builder.create();
        alert.show();
    }

    private String checkNumber(String result, boolean rigth) {
        if(Game.counter>4&&!rigth)
            return "Ваш выигрышь 100 000";
        else return result;
    }


    public void showDialog(int[] peopleAnswers){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Результаты голосования")
                .setMessage(showPeopleAnswers(peopleAnswers))
                .setCancelable(false)
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();
    }




    public String showPeopleAnswers(int[] peopleAnswers){

        ArrayList<String> peopleVoice = new ArrayList<>();
        for(int i = 0; i < game.getQuestion().getAnswer().length; i++)
            peopleVoice.add(game.getQuestion().getAnswer()[i]);


        peopleVoice.remove(game.getQuestion().getTrueAnswer()-1);

        return peopleVoice.get(0)+"  "+peopleAnswers[1]+"% \n"+
                peopleVoice.get(1)+"  "+peopleAnswers[2]+"%  \n"+
                peopleVoice.get(2)+"  "+peopleAnswers[3]+"%  \n"+
                game.getQuestion().getTrueAnswerText()+"  "+peopleAnswers[0]+"%";

    }





    public View getTrueAnswer() {
        return trueAnswer;
    }

    public View getSelected() {
        return selected;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }
}